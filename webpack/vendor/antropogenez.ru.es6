export function formatAncientDate(years, showEon) {
  var eon = " н.э.";
  if (years < 0) eon = " до н.э.";

  var title = " тыс.";
  var y = Math.abs(years) / 1000;

  if (y >= 1000) {
    y = y / 1000.0;
    title = " млн.";
  }

  var formatedYear = y.toString();

  if (Math.round(y) != y) formatedYear = y.toFixed(2);

  var result = formatedYear + title;

  if (showEon) result += eon;

  return result;
}
