import Turbolinks from "turbolinks";
Turbolinks.start();
import "./application.sass";

import "flash";

import "./pages";

import Rails from "rails-ujs";
Rails.start();
window.$ = $;
window.jQuery = jQuery;
window.kaka = $;
import axios from "axios";

document.addEventListener("turbolinks:load", function() {
  axios.defaults.headers.common["X-CSRF-Token"] = $(
    "meta[name=csrf-token]"
  ).attr("content");
  axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
});

import "magnific-popup";
import "magnific-popup/dist/magnific-popup.css";
