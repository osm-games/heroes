import "./index.sass";
import tmpl from "./question.pug";
import rtmpl from "./results.pug";
function onStateChange(event) {
  console.log(
    "player state change",
    event,
    event.target.getDuration(),
    event.target.getCurrentTime()
  );
}

function showQuestion(question) {
  const content = tmpl(question);
  const quiz = $(".video-quiz");
  quiz.html(content);
  quiz.show();
  return new Promise((resolve, reject) => {
    $(".quiz__answer").on("click", function() {
      const $t = $(this);
      question.result = $t.data("value");
      quiz.hide();
      resolve(question.result);
    });
  });
}

function showResults(results) {
  const content = rtmpl(results);
  const quiz = $(".video-quiz");
  quiz.html(content);
  quiz.show();
}

function track(player, data) {
  return new Promise((resolve, reject) => {
    let int = setInterval(function() {
      if (!player.getCurrentTime) return;
      const t = player.getCurrentTime();
      data.forEach(question => {
        if (question.show_at > t || question.shown || question.showing) {
          return;
        } else {
          player.pauseVideo();
          question.showing = true;
          setTimeout(() => {
            showQuestion(question).then(result => {
              player.playVideo();
              question.shown = true;
              question.showing = false;
              question.result = result;
            });
          }, 0);
        }
      });
      if (data.reduce((s, a) => s && a.shown, true)) {
        clearInterval(int);
        const result = data.reduce((s, a) => s + a.result, 0);
        const max = data.reduce((s, a) => s + a.max_value, 0);
        resolve({ max, result });
        return;
      }
    }, 1000);
  });
}
function init() {
  const $root = $(".videos-show");
  if ($root.length === 0) return;

  window.onYouTubeIframeAPIReady = function() {
    $(".youtube-video").each(function() {
      const $t = $(this);
      const data = $t.data("quiz");

      const player = new YT.Player($t[0], {
        height: 600,
        width: 800,
        videoId: $t.data("id")
      });
      track(player, data).then(results => {
        player.pauseVideo();
        setTimeout(() => {
          showResults(results);
        }, 0);
      });
    });
  };

  if (typeof YT === "undefined" || typeof YT.Player === "undefined") {
    //onYouTubeIframeAPIReady() will be called automatically when new tab is opening
    var tag = document.createElement("script");
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName("script")[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  } else {
    onYouTubeIframeAPIReady();
  }
}

$(document).on("turbolinks:load", init);
