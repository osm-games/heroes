import "leaflet";
import "leaflet.heat";
import "leaflet/dist/leaflet.css";
import "leaflet.animatedmarker/src/AnimatedMarker.js";
import "polyline-encoded";
import "./index.sass";
import axios from "axios";
import itemTemplate from "~/item/template.pug";
import { formatAncientDate } from "~/vendor/antropogenez.ru";
import noUiSlider from "nouislider";
import "nouislider/distribute/nouislider.css";
import maxBy from "lodash.maxby";
import minBy from "lodash.minby";

const initSlider = (lMap, min, max) => {
  console.log(min, max);
  const $root = $(".home__range_slider");
  if ($root.length === 0) return;
  const slider = noUiSlider.create($root[0], {
    start: [min, max],
    connect: true,
    range: { min, max },
    pips: {
      mode: "values",
      values: [
        -7000000,
        -6000000,
        -5000000,
        -4000000,
        -3000000,
        -2000000,
        -1000000,
        max
      ],
      format: {
        to: function(v) {
          return formatAncientDate(v, false);
        }
      },
      density: 4
    }
  });
  slider.on("set", (a, b, selection) => {
    renderItems(lMap, selection[0], selection[1]);
  });
};

const heroIcon = url =>
  L.icon({
    iconUrl: url,
    iconSize: [40, 50]
  });

let currentHero = null;
let currentHeroMarker = null;
let markers = null;

// sideEffect currentHero
const loadHeroes = lMap => {
  axios
    .get("/api/heroes")
    .then(r => {
      for (const hero of r.data) {
        currentHeroMarker = L.marker([+hero.lat, +hero.lng], {
          icon: heroIcon(hero.icon_head)
        });
        currentHeroMarker.addTo(lMap);
        currentHero = hero;
      }
      console.log(r.data);
    })
    .catch(e => {
      //console.error(e);
    });
};

let items;
const renderItems = (lMap, min, max) => {
  if (markers) {
    markers.clearLayers();
  } else {
    markers = L.layerGroup().addTo(lMap);
  }
  for (const item of items) {
    if (item.date < min || item.date > max) continue;
    const latlng = L.latLng(+item.latitude, +item.longitude);
    L.marker(latlng, {
      icon: L.icon({ iconUrl: "/square-solid.svg", iconSize: [10, 10] })
    })
      .addTo(markers)
      .on("click", () => {
        L.popup()
          .setLatLng(latlng)
          .setContent(itemTemplate(item))
          .openOn(lMap);
      });
  }
};

const loadItems = lMap => {
  axios
    .get("/api/items")
    .then(r => {
      items = r.data;
      const min = minBy(r.data, "date").date;
      const max = maxBy(r.data, "date").date;
      renderItems(lMap, min, max);
      initSlider(lMap, min, max);
    })
    .catch(e => {
      console.error(e);
    });
};

let currentRoute = null;
let currentAnim = null;

// sideEffect currentRoute, currentAnim
const showRoute = (from, to, lMap) => {
  if (!from) return;
  const query = {
    from: { lat: +from.lat, lng: +from.lng },
    to: { lat: +to.lat, lng: +to.lng }
  };
  axios
    .post("/api/routes", query)
    .then(r => {
      console.log(r.data);
      for (const route of r.data.routes) {
        if (currentRoute) {
          lMap.removeLayer(currentRoute);
        }
        if (currentAnim) {
          lMap.removeLayer(currentAnim);
        }

        currentRoute = L.Polyline.fromEncoded(route.geometry);
        currentRoute.addTo(lMap);
        const latlngs = currentRoute.getLatLngs();
        const endLatLng = latlngs[latlngs.length - 1];
        currentAnim = L.animatedMarker(currentRoute.getLatLngs(), {
          icon: heroIcon(currentHero.icon_head),
          autoStart: false,
          distance: 10,
          interval: 20,
          onEnd: () => {
            axios
              .put(`/api/heroes/${currentHero.id}`, { hero: endLatLng })
              .then(r => {
                lMap.removeLayer(currentAnim);
                currentAnim = null;
                lMap.removeLayer(currentRoute);
                currentRoute = null;
                loadHeroes(lMap);
              })
              .catch(e => {
                console.error(e);
              });
          }
        });
        currentAnim.addTo(lMap);
        break;
      }
    })
    .catch(e => {
      console.error(e);
    });
};

const moveAlong = (hero, route, lMap) => {
  curentAnim.start();
};

const init = () => {
  let $map = $("#map_canvas");
  let $root = $map;
  if ($map.length === 0) return;
  const lat = $root.data("lat") || 30;
  const lng = $root.data("lng") || 46;
  const scale = $root.data("scale") || 3;

  const lMap = L.map("map_canvas").setView([lat, lng], scale);

  //const tiles = L.tileLayer(
  //"http://a.rocketmap.ru/tile/{z}/{x}/{y}.png",
  //{}
  //).addTo(lMap);

  const tiles = L.tileLayer("http://{s}.tile.osm.org/{z}/{x}/{y}.png", {
    attribution:
      '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(lMap);

  lMap.on("moveend", e => {
    const coords = lMap.getCenter();
    coords.scale = lMap.getZoom();
    const { lat, lng, scale } = coords;
    window.history.pushState(
      coords,
      $("title").text(),
      `${window.location.pathname}?lat=${lat}&lng=${lng}&scale=${scale}`
    );
  });
  lMap.on("click", e => {
    if (currentRoute) {
      const llgs = currentRoute.getLatLngs();
      const dist = L.CRS.Simple.distance(e.latlng, llgs[llgs.length - 1]);
      console.log(dist);
      if (dist < 0.001) {
        currentAnim.start();
        lMap.removeLayer(currentHeroMarker);
        return;
      }
    }
    showRoute(currentHero, e.latlng, lMap);
  });
  lMap.on("dblclick", e => {
    return false;
    //moveAlong(currentHero, currentRoute, lMap);
  });
  lMap.doubleClickZoom.disable();
  loadHeroes(lMap);
  loadItems(lMap);
};
document.addEventListener("turbolinks:load", init);
