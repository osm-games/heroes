# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.destroy_all
u = User.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password', role: :admin)
u.heroes.create! name: 'Вася', lat: 56.49024473315436, lng: 84.96002197265626
