# frozen_string_literal: true

class AddSort < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :sort, :integer
    add_column :answers, :sort, :integer
  end
end
