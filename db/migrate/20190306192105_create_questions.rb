# frozen_string_literal: true

class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.text :content
      t.references :video
      t.bigint :show_at
      t.bigint :max_value

      t.timestamps
    end
  end
end
