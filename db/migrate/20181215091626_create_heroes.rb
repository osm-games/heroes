# frozen_string_literal: true

class CreateHeroes < ActiveRecord::Migration[5.2]
  def change
    create_table :heroes do |t|
      t.string :name
      t.references :user, foreign_key: true
      t.decimal :lat, scale: 10, precision: 15
      t.decimal :lng, scale: 10, precision: 15

      t.timestamps
    end
  end
end
