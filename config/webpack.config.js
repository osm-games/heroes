"use strict";

var path = require("path");
var webpack = require("webpack");
var ManifestPlugin = require("webpack-manifest-plugin");

var autoprefixer = require("autoprefixer");
var CompressionPlugin = require("compression-webpack-plugin");

var host = process.env.HOST || "localhost";
var devServerPort = 5709;

var production = process.env.NODE_ENV === "production";

const ExtractCssChunks = require("extract-css-chunks-webpack-plugin");

class CleanUpExtractCssChunks {
  shouldPickStatChild(child) {
    return child.name.indexOf("extract-css-chunks-webpack-plugin") !== 0;
  }

  apply(compiler) {
    compiler.hooks.done.tap("CleanUpExtractCssChunks", stats => {
      const children = stats.compilation.children;
      if (Array.isArray(children)) {
        // eslint-disable-next-line no-param-reassign
        stats.compilation.children = children.filter(child =>
          this.shouldPickStatChild(child)
        );
      }
    });
  }
}

function getStyleLoaders(isCss) {
  var loaders = [
    ExtractCssChunks.loader,
    {
      loader: "css-loader"
    },
    {
      loader: "sass-loader"
    }
  ];

  if (!isCss) {
    loaders.push({
      loader: "sass-resources-loader",
      options: {
        resources: path.resolve(
          __dirname,
          "..",
          "webpack",
          "common",
          "index.sass"
        )
      }
    });
  }

  return loaders;
}

var config = {
  mode: production ? "production" : "development",
  entry: {
    application: "application.es6"
  },

  module: {
    rules: [
      { test: /\.pug$/, use: "pug-loader" },
      { test: /\.es6$/, use: "babel-loader" },
      { test: /\.(jpe?g|png|gif)$/i, use: "file-loader" },
      {
        test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)|\.otf($|\?)/,
        use: "file-loader"
      },
      {
        test: /\.(scss|css)$/,
        use: getStyleLoaders(true)
      },
      {
        test: /\.sass$/,
        use: getStyleLoaders(false)
      }
    ]
  },

  output: {
    path: path.join(__dirname, "..", "public", "webpack"),

    filename: production ? "[name]-[chunkhash].js" : "[name].js",
    chunkFilename: production ? "[name]-[chunkhash].js" : "[name].js"
  },

  resolve: {
    modules: [
      path.resolve(__dirname, "..", "webpack"),
      path.resolve(__dirname, "..", "node_modules")
    ],
    extensions: [".es6", ".sass", ".css", ".js"],
    alias: {
      "~": path.resolve(__dirname, "..", "webpack")
      //react: "preact"
    }
  },

  plugins: [
    new webpack.DefinePlugin({
      // <--key to reduce React's size
      "process.env": {
        NODE_ENV: JSON.stringify(production ? "production" : "development")
      }
    }),
    new ExtractCssChunks({
      filename: "[name].css",
      chunkFilename: "[id].css",
      hot: production ? false : true
    }),
    new CleanUpExtractCssChunks(),
    new ManifestPlugin({
      writeToFileEmit: true,
      //basePath: "",
      publicPath: production
        ? "/webpack/"
        : "http://" + host + ":" + devServerPort + "/webpack/"
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
      "window.$": "jquery"
      //Popper: ["popper.js", "default"]
    })
  ],

  optimization: {
    minimize: production,
    splitChunks: {
      cacheGroups: {
        default: false,
        vendors: {
          test: /[\\/]node_modules[\\/].*js/,
          priority: 1,
          name: "npm",
          chunks: "initial",
          enforce: true
        }
      }
    }
  }
};

if (production) {
  config.plugins.push(
    new CompressionPlugin({
      //asset: "[path].gz",
      algorithm: "gzip",
      test: /\.js$|\.css$/,
      threshold: 4096,
      minRatio: 0.8
    })
  );
  config.output.publicPath = "https://progress.pmisurvey.ru/webpack/";
} else {
  config.plugins.push(new webpack.NamedModulesPlugin());

  config.devServer = {
    port: devServerPort,
    headers: { "Access-Control-Allow-Origin": "*" }
  };

  config.output.publicPath =
    "http://" + host + ":" + devServerPort + "/webpack/";
  // Source maps
  config.devtool = "source-map";
}

module.exports = config;
