# frozen_string_literal: true

set :stage, :production
set :rails_env, :production

server 'shirykalov.ru', user: 'heroes', roles: %w[web app db]
