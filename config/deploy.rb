# frozen_string_literal: true

set :user, 'heroes'
set :application, 'heroes'

set :repo_url, 'git@gitlab.com:osm-games/heroes.git'
set :branch, ENV['REVISION'] || 'master'
# set :default_env, {
#  'REDIS_URL' => 'redis://localhost:6379/1'
# }

set :deploy_to, -> { "/home/#{fetch(:user)}/app" }

set :puma_state, -> { File.join(shared_path, 'tmp', 'puma', 'state') }
set :puma_pid, -> { File.join(shared_path, 'tmp', 'puma', 'pid') }
set :puma_conf, -> { File.join(current_path, 'config', 'puma.rb') }

# set :sidekiq_concurrency, 5 # кол-во тредов на процесс, синхронизировано с pool в active_record

Rake::Task['puma:check'].clear
Rake::Task['puma:config'].clear
namespace :puma do
  task :check do
    puts 'override'
  end
  task :config do
  end
end

set :keep_releases, 10

set :use_sudo, :false

# set :linked_files, %w(config/database.yml config/secrets.yml public/robots.txt public/sitemap.xml config/puma.rb)
set :linked_files, %w[config/database.yml config/secrets.yml]
set :linked_dirs, %w[log tmp vendor/bundle public/assets public/system public/uploads node_modules storage public/webpack]
