# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  namespace :api do
    resources :heroes, only: %i[index show update]
    resources :items, only: :index
    post 'routes' => 'routes#search'
  end
  resources :videos, only: :show
  root to: 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
