# frozen_string_literal: true

ActiveAdmin.register Video do
  permit_params :url,
                questions_attributes: %i[id content sort show_at max_value _destroy] + [{
                  answers_attributes: %i[id content sort value _destroy]
                }]
  form do |f|
    f.inputs do
      f.input :url
      f.has_many :questions, sortable: :sort, allow_destroy: true do |qf|
        qf.input :content
        qf.input :show_at
        qf.input :max_value
        qf.has_many :answers, sortable: :sort, allow_destroy: true do |af|
          af.input :content
          af.input :value
        end
      end
    end
    f.actions
  end
end
