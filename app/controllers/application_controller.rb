# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery
  def access_denied(exception)
    redirect_to root_path, flash: { error: exception.message }
  end

  helper_method :page_title

  def page_title
    'Герои'
  end
end
