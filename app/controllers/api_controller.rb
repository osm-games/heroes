# frozen_string_literal: true

class ApiController < ActionController::API
  before_action :authenticate_user!
end
