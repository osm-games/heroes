# frozen_string_literal: true

class VideosController < ApplicationController
  def show
    @video = Video.preload(questions: :answers).find(params[:id])
  end
end
