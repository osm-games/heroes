# frozen_string_literal: true

class HomeController < ApplicationController
  def index
    @coords = params.slice(:scale, :lat, :lng, :precision, :my)
  end
end
