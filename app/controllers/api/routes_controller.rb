# frozen_string_literal: true

class Api::RoutesController < ApplicationController
  # POST /api/routes
  def search
    h = Rails.application.secrets.osrm_host
    u = URI.parse(h) + "/route/v1/driving/#{search_params}"
    r = HTTParty.get(u.to_s, query: { overview: :full, geometries: :polyline })
    if r.code != 200
      puts r.body
      render json: { error: 'remote error' }, status: r.code
      return
    end
    render json: JSON.parse(r.body)
  end

  def search_params
    %i[from to].map do |k|
      %i[lng lat].map do |kk|
        params[k][kk]
      end.join(',')
    end.join(';')
  end
end
