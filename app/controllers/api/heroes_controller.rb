# frozen_string_literal: true

class Api::HeroesController < ApiController
  def index
    render json: current_user.heroes
  end

  def show
    render json: current_user.heroes.find(params[:id])
  end

  def update
    current_user.heroes.find(params[:id]).update(hero_params)
  end

  private

  def hero_params
    params.require(:hero).permit(:lat, :lng)
  end
end
