# frozen_string_literal: true

class Api::ItemsController < ApiController
  skip_before_action :authenticate_user!
  def index
    r = HTTParty.get('http://antropogenez.ru/api.php?v=1&t=locations')
    if r.code != 200
      render json: {code: r.code, body: r.body}, status: 502
      return
    end
    render json: r.parsed_response
  end
end
