# frozen_string_literal: true

class Video < ApplicationRecord
  has_many :questions, dependent: :destroy
  accepts_nested_attributes_for :questions, allow_destroy: true

  # ready_for_quez? возвращает готово ли видео для показа пользователю
  # оно не готово, если есть вопрос без ответов или без правильного ответа
  # ответ правильный, если value > 0
  def ready_for_quiz?
    questions.map { |q| q.answers.map(&:value).max }.min.positive?
  end

  validate :valid_url?

  YOUTUBE_REGEXP = %r{^(?:https?://)?(?:www\.)?youtu(?:\.be|be\.com)/(?:watch\?v=)?([\w-]{10,})}i.freeze

  def youtube?
    return false if url.nil?

    !url.match(YOUTUBE_REGEXP).nil?
  end

  def youtube_id
    return nil if url.blank?

    m = url.match(YOUTUBE_REGEXP)
    return nil if m.nil?

    m[1]
  end

  def valid_url?
    errors.add(:url, 'Не корректная ссылка') unless youtube?
  end
end
