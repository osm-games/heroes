# frozen_string_literal: true

class Hero < ApplicationRecord
  belongs_to :user
  has_one_attached :icon_head
  def as_json(_options = {})
    attributes.merge(
      icon_head: Rails.application.routes.url_helpers.rails_blob_path(icon_head, only_path: true)
    )
  end
end
