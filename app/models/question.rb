# frozen_string_literal: true

class Question < ApplicationRecord
  belongs_to :video
  has_many :answers, dependent: :destroy
  accepts_nested_attributes_for :answers, allow_destroy: true
  def as_json(_options = {})
    attributes.merge(
      answers: answers.as_json
    )
  end
end
