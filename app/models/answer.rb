# frozen_string_literal: true

class Answer < ApplicationRecord
  belongs_to :question
  def as_json(_options = {})
    attributes
  end
end
