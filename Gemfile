# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

gem 'activeadmin'
gem 'activeadmin_addons'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'devise'
gem 'enumerize'
gem 'httparty'
gem 'jbuilder', '~> 2.5'
gem 'kaminari'
gem 'pg', '>= 0.18', '< 2.0'
gem 'puma', github: 'AnatolyShirykalov/puma'
gem 'rails', '~> 5.2.2'
gem 'redis'
gem 'rs-webpack-rails'
gem 'sidekiq'
gem 'slim'
gem 'slim-rails'
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
end

group :development do
  gem 'capistrano', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano3-puma', require: false
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

gem 'cancancan', '~> 2.3'

gem "irb", "~> 1.0"
